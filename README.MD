# qsimcal

Simple calendar applet that also can show time worldwide (added by user) and add events to specific date (also added by user).

This program is part of [Flitter](https://gitlab.com/flitter-tools), all informations can be found in [this repository](https://gitlab.com/flitter-tools/General).

# License

See LICENSE file in repository root.
