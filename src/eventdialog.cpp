#include "eventdialog.h"
#include "ui_eventdialog.h"

#include <QDesktopWidget>
#include <QSettings>
#include <QDir>

EventDialog::EventDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EventDialog)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Popup);

    this->move(qApp->desktop()->height() / 2 - this->height() / 2, qApp->desktop()->width() / 2 - this->width() / 2);
}

EventDialog::~EventDialog()
{
    delete ui;
}

void EventDialog::setDate(const QDate &date)
{
    this->date = date;

    QSettings settings(QDir::homePath() + "/.config/flitter/calendar.conf", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");
    ui->le_event->setText(settings.value(date.toString()).toString());
}

void EventDialog::on_btn_ok_clicked()
{
    QSettings settings(QDir::homePath() + "/.config/flitter/calendar.conf", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");

    if (ui->le_event->text().isEmpty())
        settings.remove(date.toString());
    else
        settings.setValue(date.toString(), ui->le_event->text());

    this->close();
}

void EventDialog::on_btn_cancel_clicked()
{
    this->close();
}
