#ifndef EVENTDIALOG_H
#define EVENTDIALOG_H

#include <QDialog>
#include <QDate>

namespace Ui {
class EventDialog;
}

class EventDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit EventDialog(QWidget *parent = nullptr);
        ~EventDialog();

        void setDate(const QDate &date);

    private slots:
        void on_btn_ok_clicked();
        void on_btn_cancel_clicked();

private:
        Ui::EventDialog *ui;
        QDate date;
};

#endif // EVENTDIALOG_H
