#include "newtimedialog.h"
#include "ui_newtimedialog.h"

#include <QDesktopWidget>
#include <QSettings>
#include <QDir>

NewtimeDialog::NewtimeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewtimeDialog)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Popup);

    this->move(qApp->desktop()->height() / 2 - this->height() / 2, qApp->desktop()->width() / 2 - this->width() / 2);
}

NewtimeDialog::~NewtimeDialog()
{
    delete ui;
}

void NewtimeDialog::on_buttonBox_accepted()
{
    if (!ui->le_label->text().isEmpty())
        city = ui->le_label->text();
    else
        city = "Unknown";

    offset = ui->comb_offset->currentText().toInt();

    QSettings settings(QDir::homePath() + "/.config/flitter/calendar.conf", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Times");
    int i = settings.allKeys().count();
    settings.setValue(QString::number(i+1), city + "," + QString::number(offset));
    settings.endGroup();

    this->close();
}

void NewtimeDialog::on_buttonBox_rejected()
{
    this->close();
}

QString NewtimeDialog::getCity() const
{
    return city;
}

int NewtimeDialog::getOffset() const
{
    return offset;
}
