#ifndef NEWTIMEDIALOG_H
#define NEWTIMEDIALOG_H

#include <QDialog>

namespace Ui {
class NewtimeDialog;
}

class NewtimeDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit NewtimeDialog(QWidget *parent = nullptr);
        ~NewtimeDialog();

        QString getCity() const;
        int getOffset() const;

    private slots:
        void on_buttonBox_accepted();
        void on_buttonBox_rejected();

    private:
        Ui::NewtimeDialog *ui;

        QString city;
        int offset;
};

#endif // NEWTIMEDIALOG_H
