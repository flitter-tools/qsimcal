#include <QApplication>
#include "calendarpopup.h"
#include "sigwatch.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    CalendarPopup popup;
    //popup.show();

    UnixSignalWatcher sigwatch;
    sigwatch.watchForSignal(SIGUSR1);
    sigwatch.watchForSignal(SIGTERM);
    sigwatch.watchForSignal(SIGINT);
    QObject::connect(&sigwatch, &UnixSignalWatcher::unixSignal, [&](int signal){
        if (signal == SIGUSR1)
            popup.show();

        if (signal == SIGTERM or signal == SIGINT)
            app.quit();
    });

    return app.exec();
}
