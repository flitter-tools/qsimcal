#ifndef CALENDARPOPUP_H
#define CALENDARPOPUP_H

#include <QDialog>
#include <QApplication>
#include <QDesktopWidget>

#include "mycalendar.h"
#include "newtimedialog.h"

namespace Ui {
class CalendarPopup;
}

class CalendarPopup : public QDialog
{
    Q_OBJECT

    public:
        explicit CalendarPopup(QWidget *parent = nullptr);
        ~CalendarPopup();

    protected slots:
        void hideEvent(QHideEvent *event);

    signals:
        void hide();

    private slots:
        void showCurrDay(const QDate &date);

    private:
        MyCalendar *calendar;
        Ui::CalendarPopup *ui;

        NewtimeDialog *dialog;

        void populateTimes();
};

#endif // CALENDARPOPUP_H
