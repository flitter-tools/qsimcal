#ifndef MYCALENDAR_H
#define MYCALENDAR_H

#include <QObject>
#include <QCalendarWidget>
#include <QPen>
#include <QDate>
#include <QBrush>

#include "eventdialog.h"

class MyCalendar : public QCalendarWidget
{
    Q_OBJECT

    public:
        explicit MyCalendar(QCalendarWidget *parent = nullptr);
        ~MyCalendar();

        void setColor(QColor& color);
        QColor getColor();

    protected:
        virtual void paintCell(QPainter *painter, const QRect &rect, const QDate &date) const;

    private slots:
        void CalendarDBClick(const QDate& Date);

    private:
        void showCurrDay();

        QDate m_currentDate;
        QPen m_outlinePen;
        QBrush m_transparentBrush;
        EventDialog *dialog;
        QStringList events;
};

#endif // MYCALENDAR_H
