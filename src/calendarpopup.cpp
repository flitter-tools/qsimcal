#include "calendarpopup.h"
#include "ui_calendarpopup.h"
#include "lwcustomitem.h"

#include <QSettings>
#include <QDir>
#include <QPushButton>

CalendarPopup::CalendarPopup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalendarPopup)
{
    ui->setupUi(this);

    calendar = new MyCalendar;

    this->setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);
    this->setFixedSize(QSize(320, 250));

    // todo: lib that parse tint2 config and find out where is panel now and it's height
    this->move(QApplication::desktop()->width() - this->width(),\
               QApplication::desktop()->height() - this->height() - 25);

    ui->la_calendar->addWidget(calendar);

    connect(calendar, &MyCalendar::clicked, this, &CalendarPopup::showCurrDay);

    populateTimes();

    dialog = new NewtimeDialog;
}

CalendarPopup::~CalendarPopup()
{
    delete dialog;
    delete calendar;
    delete ui;
}

void CalendarPopup::hideEvent(QHideEvent *event)
{
    if (event)
        this->hide();
        //QApplication::quit();
}

void CalendarPopup::showCurrDay(const QDate &date)
{
    QSettings settings(QDir::homePath() + "/.config/flitter/calendar.conf", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");
    QString event = settings.value(date.toString()).toString();
    ui->l_currentdate->setText(event);
}

void CalendarPopup::populateTimes()
{
    QListWidgetItem *item = new QListWidgetItem(ui->lw_time);
    LWCustomItem *citem = new LWCustomItem(this, 0, "Your city");

    item->setSizeHint(citem->sizeHint());

    ui->lw_time->setItemWidget(item, citem);

    QSettings settings(QDir::homePath() + "/.config/flitter/calendar.conf", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");
    settings.beginGroup("Times");
    for (const auto &key : settings.allKeys())
    {
        QStringList tmp = settings.value(key).toString().split(',');

        QListWidgetItem *item = new QListWidgetItem(ui->lw_time);
        LWCustomItem *citem = new LWCustomItem(this, tmp[1].toInt(), tmp[0]);

        item->setSizeHint(citem->sizeHint());

        ui->lw_time->setItemWidget(item, citem);
    }
    settings.endGroup();

    QListWidgetItem *itembtn = new QListWidgetItem(ui->lw_time);
    QPushButton *btn = new QPushButton("+");

    connect(btn, &QPushButton::clicked, [&]{
        dialog->exec();
        QString city = dialog->getCity();
        int offset = dialog->getOffset();

        QListWidgetItem *item = new QListWidgetItem(ui->lw_time);
        LWCustomItem *citem = new LWCustomItem(this, offset, city);

        item->setSizeHint(citem->sizeHint());

        ui->lw_time->setItemWidget(item, citem);
    });


    itembtn->setSizeHint(btn->sizeHint());

    ui->lw_time->setItemWidget(itembtn, btn);
}
