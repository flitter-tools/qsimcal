#include "popup.h"

popup::popup(QWidget *parent) : QWidget(parent)
{
    this->setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);
    this->setFixedSize(QSize(250, 250));

    // todo: lib that parse tint2 config and find out where is panel now and it's height
    this->move(QApplication::desktop()->width() - this->width(),\
               QApplication::desktop()->height() - this->height() - 25);

    create();
}

void popup::hideEvent(QHideEvent *event)
{
    if (event)
        QApplication::quit();
}

void popup::create()
{
    this->layout()->addWidget(new MyCalendar);
}
