#ifndef LWCUSTOMITEM_H
#define LWCUSTOMITEM_H

#include <QWidget>
#include <QLabel>
#include <QDateTime>
#include <QVBoxLayout>

class LWCustomItem : public QWidget
{
    Q_OBJECT

    public:
        explicit LWCustomItem(QWidget *parent = nullptr, const int &offset = 0, const QString &city = "Moscow");
        virtual ~LWCustomItem();

    private slots:
        void updateTime();

    private:
        QLabel *l_time;
        QLabel *l_city;
        QVBoxLayout *layout;

        int m_offset;
};

#endif // LWCUSTOMITEM_H
