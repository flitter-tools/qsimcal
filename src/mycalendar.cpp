#include "mycalendar.h"

#include <QSettings>
#include <QDir>
#include <QPainter>

MyCalendar::MyCalendar(QCalendarWidget *parent) : QCalendarWidget(parent)
{
    m_currentDate = QDate::currentDate();
    m_outlinePen.setColor(Qt::red);
    m_transparentBrush.setColor(Qt::transparent);

    this->setFirstDayOfWeek(Qt::Monday);
    this->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);

    connect(this, &MyCalendar::activated, this, &MyCalendar::CalendarDBClick);

    dialog = new EventDialog;

    QSettings settings(QDir::homePath() + "/.config/flitter/calendar.conf", QSettings::IniFormat);
    settings.setIniCodec("UTF-8");
    events = settings.allKeys();
}

MyCalendar::~MyCalendar()
{
    delete dialog;
}

void MyCalendar::CalendarDBClick(const QDate& Date)
{
    dialog->setDate(Date);
    dialog->show();
}

void MyCalendar::setColor(QColor& color)
{
   m_outlinePen.setColor(color);
}

QColor MyCalendar::getColor()
{
   return (m_outlinePen.color());
}

void MyCalendar::paintCell(QPainter *painter, const QRect &rect, const QDate &date) const
{
   QCalendarWidget::paintCell(painter, rect, date);

   if (events.contains(date.toString()) || date == m_currentDate)
   {
       painter->setPen(m_outlinePen);
       painter->setBrush(m_transparentBrush);
       painter->drawRect(rect.adjusted(0, 0, -1, -1));
   }
}
