#include "lwcustomitem.h"
#include <QTimer>
#include <QTime>

LWCustomItem::LWCustomItem(QWidget *parent, const int &offset, const QString &city) : QWidget(parent)
{
    layout = new QVBoxLayout;
    l_time = new QLabel;
    l_city = new QLabel;

    l_time->setStyleSheet("font-size: 17px;");

    m_offset = offset;

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &LWCustomItem::updateTime);
    timer->start(1000);

    updateTime();

    l_city->setText(city);

    layout->addWidget(l_city);
    layout->addWidget(l_time);

    this->setLayout(layout);
}

LWCustomItem::~LWCustomItem()
{
    delete l_city;
    delete l_time;
    delete layout;
}

void LWCustomItem::updateTime()
{
    QTime now = QTime::currentTime();
    l_time->setText(now.addSecs(m_offset*60*60).toString());
}
